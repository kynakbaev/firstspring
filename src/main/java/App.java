import entity.Bank;
import service.impl.BankServiceImpl;

/**
 * @autor Kunakbaev Artem
 */
public class App {

    public static void main(String[] args) {
        BankServiceImpl bank = new BankServiceImpl();
        bank.addBank(new Bank("Sberbank"));
    }
}
