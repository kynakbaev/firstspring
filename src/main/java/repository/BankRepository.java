package repository;

import entity.Bank;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankRepository extends JpaRepository<Bank, Long> {
//    @Query("select b from Bank b where b.name = :name")
//    Bank findByName(@Param("name") String name);
}
