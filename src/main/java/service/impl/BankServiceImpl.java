package service.impl;

import entity.Bank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.BankRepository;
import service.BankService;

import java.util.List;

/**
 * @autor Kunakbaev Artem
 */
@Service
public class BankServiceImpl implements BankService {

    @Autowired
    private BankRepository bankRepository;

    public Bank addBank(Bank bank) {
        return bankRepository.saveAndFlush(bank);
    }

    public void delete(long id) {
        bankRepository.delete(id);
    }

    public Bank getById(long id) {
        return bankRepository.findOne(id);
    }

    public Bank editBank(Bank bank) {
        return bankRepository.saveAndFlush(bank);
    }

    public List<Bank> getAll() {
        return bankRepository.findAll();
    }
}
