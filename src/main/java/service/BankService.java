package service;

import entity.Bank;

import java.util.List;

public interface BankService {
    Bank addBank(Bank bank);

    void delete(long id);

    Bank getById(long id);

    Bank editBank(Bank bank);

    List<Bank> getAll();
}
